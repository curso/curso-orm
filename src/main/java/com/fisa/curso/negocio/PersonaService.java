package com.fisa.curso.negocio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fisa.curso.modelo.Persona;

@Stateless
public class PersonaService {
	
	@PersistenceContext
    EntityManager em;
	
	@Inject
	CrudService crudService;

	public Persona create(Persona persona)  {
		return this.em.merge(persona);
		//return crudService.save(persona);
	}

	public List<Persona> retrieve()  {
		return crudService.findWithNativeQuery("SELECT * FROM Persona ORDER BY identificacion", Persona.class);
	}
	
	public List<Persona> retrieve(String identificacion) throws Exception {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("identificacion", identificacion+"%");
		return crudService.findWithQuery(
				"SELECT o FROM Persona o LEFT JOIN FETCH o.contactos "
				+ " WHERE o.identificacion "
				+ " LIKE :identificacion "
				+ " ORDER BY o.identificacion ", parametros);
				
	}
	
	public Persona findById(String identificacion){
		return crudService.find(Persona.class, identificacion);
	}

}
