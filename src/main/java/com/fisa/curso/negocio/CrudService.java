package com.fisa.curso.negocio;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Basado en el servicio descrito por Adam Bien
 * http://www.adam-bien.com/roller/abien/entry/generic_crud_service_aka_dao
 *
 * @author gersonZaragocin
 *
 *
 */
@Stateless
public class CrudService {

    @PersistenceContext
    EntityManager em;


    public <T> T save(T t) {
        return (T) this.em.merge(t);
    }

    public <T> T find(Class<T> type, Object id) {
        return (T) this.em.find(type, id);
    }

    public <T> void delete(Class<T> type, Object id) {
        Object ref = this.em.getReference(type, id);
        this.em.remove(ref);
    }

    public <T> List<T> findWithQuery(String queryString) {
        Query query = this.em.createQuery(queryString);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findWithQuery(String queryString, Map<String, Object> parameters) {
        Query query = this.em.createQuery(queryString);
        setParameters(query, parameters, 0);
        return query.getResultList();
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName) {
        return this.em.createNamedQuery(namedQueryName).getResultList();
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
        return findWithNamedQuery(namedQueryName, parameters, 0);
    }

    public <T> List<T> findWithNamedQuery(String queryName, int resultLimit) {
        return this.em.createNamedQuery(queryName).setMaxResults(resultLimit)
                .getResultList();
    }

    public <T> List<T> findWithNativeQuery(String sql, Class<T> type) {
        return this.em.createNativeQuery(sql, type).getResultList();
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters,
            int resultLimit) {

        Query query = this.em.createNamedQuery(namedQueryName);
        setParameters(query, parameters, resultLimit);

        return query.getResultList();
    }

    public <T> T findSingleWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters) {
        List<T> results = findWithNamedQuery(namedQueryName, parameters, 0);
        if (results.isEmpty()) {
            return null;
        } else if (results.size() == 1) {
            return results.get(0);
        }
        throw new NonUniqueResultException();
    }

    private void setParameters(Query query, Map<String, Object> parameters, int resultLimit) {
        Set<Entry<String, Object>> rawParameters = parameters.entrySet();
        if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }
        for (Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
    }
}
