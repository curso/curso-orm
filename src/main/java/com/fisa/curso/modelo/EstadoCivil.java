package com.fisa.curso.modelo;

public enum EstadoCivil {
	C,
	S,
	D,
	V,
	U
}
