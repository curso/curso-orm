package com.fisa.curso.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Persona {
	@Id
	String identificacion;
	
	@Column(name="PER_NOMBRE",length=50)
	String nombre;
	
	@Column(length=50)
	String apellido;
	
	@Enumerated(EnumType.STRING)
	@Column(length=1)
	Genero genero;
	
	@Enumerated(EnumType.STRING)
	@Column(length=1)
	EstadoCivil estadoCivil;
	
	@OneToMany(targetEntity=Contacto.class, fetch=FetchType.EAGER)
	@JoinColumn(name="persona_id")
	List<Contacto> contactos;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}	
	
	@Override
	public String toString() {
		String valor = identificacion+"\t"+nombre + "\t"+ apellido+"\t"+estadoCivil+"\t"+genero;
		return valor;
	}
}
